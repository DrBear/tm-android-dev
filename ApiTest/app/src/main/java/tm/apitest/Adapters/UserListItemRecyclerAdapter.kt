package tm.apitest.Adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import tm.apitest.Holders.UserListItemHolder
import tm.apitest.R
import tm.apitest.Models.DatabaseUser

class UserListItemRecyclerAdapter : RecyclerView.Adapter<UserListItemHolder>() {

    var currentList : ArrayList<DatabaseUser> = ArrayList()

    override fun getItemCount(): Int {
        return currentList.size
    }

    override fun onBindViewHolder(holder: UserListItemHolder, position: Int) {
        var databaseUser = currentList[position]
        holder.UpdateWithDatabaseUser(databaseUser)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserListItemHolder {
        var cardItem = LayoutInflater.from(parent.context).inflate(R.layout.userslist_layout, parent, false)
        return UserListItemHolder(cardItem)
    }

}