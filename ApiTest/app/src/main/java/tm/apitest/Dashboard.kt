package tm.apitest

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.app_bar_dashboard.*
import com.auth0.android.jwt.JWT
import tm.apitest.Fragments.UserListFragment


class Dashboard : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private var _clientMail:String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }
        _clientMail = GetUserEmail()
        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        nav_view.setNavigationItemSelectedListener(this)
        val mDrawerLayout : DrawerLayout = findViewById(R.id.drawer_layout)
        mDrawerLayout.addDrawerListener(
                object : DrawerLayout.DrawerListener {
                    override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                        findViewById<TextView>(R.id.txtName).text = _clientMail
                    }

                    override fun onDrawerOpened(drawerView: View) {

                    }

                    override fun onDrawerClosed(drawerView: View) {
                        // Respond when the drawer is closed
                    }

                    override fun onDrawerStateChanged(newState: Int) {
                        // Respond when the drawer motion state changes
                    }
                }
        )
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.dashboard, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    private var _userlistfragment: UserListFragment = UserListFragment()

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        val transaction = supportFragmentManager.beginTransaction()
        transaction.setCustomAnimations(android.R.anim.fade_in,android.R.anim.fade_out)

        when (item.itemId) {
            R.id.SignOut -> {
                getSharedPreferences("TMApi", Context.MODE_PRIVATE).edit().clear().apply()
                startActivity(Intent(this, SignUp::class.java))
                finish()
            }
            R.id.users_list -> {
                this._userlistfragment = UserListFragment()
                transaction.replace(R.id.fragment_container, _userlistfragment)
            }
        }
        transaction.commit()
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    public fun GetSharedPreferences(): SharedPreferences {
        return getSharedPreferences("TMApi", Context.MODE_PRIVATE)
    }

    private fun GetUserEmail () : String?{
        val prefs=  getSharedPreferences("TMApi", Context.MODE_PRIVATE)
        val parsedJWT = JWT(prefs.getString("token",""))
        val claimMail = parsedJWT.getClaim("email")
        return claimMail.asString()
    }

}
