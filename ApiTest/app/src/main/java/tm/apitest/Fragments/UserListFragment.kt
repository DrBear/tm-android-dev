package tm.apitest.Fragments


import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.view.drags
import com.jakewharton.rxbinding2.view.scrollChangeEvents
import com.jakewharton.rxbinding2.view.touches
import tm.apitest.Models.DatabaseUser


import tm.apitest.R
import kotlin.collections.ArrayList
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import khttp.get
import khttp.post
import kotlinx.android.synthetic.main.fragment_user_list.*
import tm.apitest.Adapters.UserListItemRecyclerAdapter
import tm.apitest.Utils.ViewStateChange


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class UserListFragment : Fragment() {

    var userListRecycler: RecyclerView? = null
    var refresher: SwipeRefreshLayout? = null
    var adapter: UserListItemRecyclerAdapter = UserListItemRecyclerAdapter()
    var prefs: SharedPreferences? = null
    var token: String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_user_list, container, false)
        refresher = view.findViewById(R.id.fragment_refresher)!!
        userListRecycler = view.findViewById(R.id.users_list_recycler)
        userListRecycler!!.layoutManager = LinearLayoutManager(context)
        userListRecycler!!.setHasFixedSize(true)
        userListRecycler!!.adapter = adapter
        prefs = this.activity?.getSharedPreferences("TMApi", Context.MODE_PRIVATE)
        token = prefs?.getString("token", "")!!

        refresher!!.setOnRefreshListener {
            Observable.
                    just("")
                    .subscribeOn(Schedulers.io())
                    .switchMap { GetObserver(GetActualUserList(token))}
                    .onErrorReturn { ViewStateChange.Error(it) }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { it ->
                        when (it) {

                            is ViewStateChange.InProgress -> {
                                refresher?.isRefreshing = true
                            }
                            is ViewStateChange.Error -> {
                                Log.e("ERROR",it.error.message)
                                refresher?.isRefreshing = false
                            }
                            is ViewStateChange.Completed -> {
                                adapter.currentList.clear()
                                adapter.currentList = it.list
                                adapter.notifyDataSetChanged()
                                refresher?.isRefreshing = false
                            }
                        }
                    }
        }


        return view
    }

    private  fun GetActualUserList(accessToken:String):ArrayList<DatabaseUser>{
        val response = get("http://tournamentmanagementapi.azurewebsites.net/api/usersmanagement/getallusers/", headers = mapOf("Authorization" to "Bearer $accessToken"))
        val jsonResponse = response.jsonArray
        val listToDeserialize = jsonResponse.toString()
        val mapper = jacksonObjectMapper()
        return mapper.readValue(listToDeserialize)
    }

    private fun GetObserver (list:ArrayList<DatabaseUser>) : Observable<ViewStateChange>{
        return Observable
                .just(list)
                .map {
                    ViewStateChange.Completed(it) as ViewStateChange
                }
                .startWith(ViewStateChange.InProgress() as ViewStateChange)
                .subscribeOn(Schedulers.io())
    }

    private fun GetUserToken():String{
        return post(url = "http://tournamentmanagementapi.azurewebsites.net/api/login"
                , json = mapOf("Password" to "e", "Email" to "e"))
                .jsonObject["token"]
                .toString()
    }

    private fun UpdateCurrentList (list:ArrayList<DatabaseUser>) = this.activity?.runOnUiThread({
//        this.adapter.currentList = list
//        this.adapter.notifyDataSetChanged()
//        refresher?.isRefreshing = false
    })

}


