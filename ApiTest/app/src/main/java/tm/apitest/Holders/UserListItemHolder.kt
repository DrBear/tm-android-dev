package tm.apitest.Holders

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import tm.apitest.Models.DatabaseUser
import tm.apitest.R

class UserListItemHolder(itemView: View) :RecyclerView.ViewHolder(itemView) {
    private val userEmailTextView:TextView = itemView.findViewById(R.id.user_email)
    private val useridTextView:TextView = itemView.findViewById(R.id.user_id)
    private val userPasswordTextView:TextView = itemView.findViewById(R.id.user_password)
    private var _databaseUser : DatabaseUser? = null

    fun UpdateWithDatabaseUser(databaseUser : DatabaseUser){
        userEmailTextView.text = databaseUser.email
        useridTextView.text = databaseUser.id
        userPasswordTextView.text = databaseUser.password
        _databaseUser = databaseUser
    }
}