package tm.apitest.Models

import com.fasterxml.jackson.annotation.*

class DatabaseUser(Id:String?,Password:String?,Email:String? ) {

    @get:JsonProperty("id")
    @set:JsonProperty("id")
    var id: String? = null
    @get:JsonProperty("password")
    @set:JsonProperty("password")
    var password: String? = null
    @get:JsonProperty("email")
    @set:JsonProperty("email")
    var email: String? = null
    @get:JsonProperty("avatarPath")
    @set:JsonProperty("avatarPath")
    var avatarPath: String? = null
    @get:JsonProperty("userType")
    @set:JsonProperty("userType")
    var userType: Long = 0
    @get:JsonProperty("gameProfiles")
    @set:JsonProperty("gameProfiles")
    var gameProfiles: Array<Any>? = null
    init {
        id = Id
        password = Password
        email = Email
    }
}


