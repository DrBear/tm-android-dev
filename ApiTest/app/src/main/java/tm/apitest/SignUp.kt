package tm.apitest

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.TextView

import khttp.post
import khttp.responses.Response

import kotlinx.android.synthetic.main.activity_sign_up.*
import org.json.JSONObject

import android.content.Context

import android.content.SharedPreferences
import android.content.Intent
import android.view.View
import android.widget.ProgressBar


class SignUp : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val prefs: SharedPreferences = getSharedPreferences("TMApi", Context.MODE_PRIVATE)
        val token:String = prefs.getString("token","")
        if (token != "")
        {
            startActivity(Intent(this, Dashboard::class.java))
            finish()
        }
        else
        {
            setContentView(R.layout.activity_sign_up)
            findViewById<Button>(R.id.btnSignIn).setOnClickListener {
                GetToken()
            }
        }
    }


    private fun GetToken() {
        val email = findViewById<TextView>(R.id.txtMail).text
        val password = findViewById<TextView>(R.id.txtPassword).text
        val prefs: SharedPreferences = getSharedPreferences("TMApi", Context.MODE_PRIVATE)
        val edit: SharedPreferences.Editor
        val progress : ProgressBar = findViewById(R.id.pbarSignIn)
        var message : String = ""
        progress.visibility = View.VISIBLE
        progress.z = 999F
        edit = prefs.edit()
        val thread = Thread(Runnable {
            try {
                val response : Response = post(url ="http://tournamentmanagementapi.azurewebsites.net/api/login"
                       , json=mapOf("Password" to "$password", "Email" to "$email"))
                try
                {
                    val obj : JSONObject = response.jsonObject
                    val token = obj["token"].toString()
                    edit.putString("token", token)
                    edit.apply()
                    startActivity(Intent(this, Dashboard::class.java))
                    finish()
                }
                catch (e: Exception) {
                    runOnUiThread {
                        progress.visibility = View.INVISIBLE
                    }
                    message = response.text.replace("\"", "")
                    runOnUiThread {
                        findViewById<TextView>(R.id.txtError).text = message
                        findViewById<TextView>(R.id.txtError).visibility = View.VISIBLE
                    }
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        })

        thread.start()
    }
}
