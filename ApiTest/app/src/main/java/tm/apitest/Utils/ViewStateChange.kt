package tm.apitest.Utils

import tm.apitest.Models.DatabaseUser


sealed class ViewStateChange() {
    class InProgress() : ViewStateChange()
    data class Error(val error:Throwable) : ViewStateChange()
    data class Completed(val list:ArrayList<DatabaseUser>) : ViewStateChange()

}
