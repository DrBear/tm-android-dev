import java.io.IOException

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.*

object Converter {

    private var reader: ObjectReader? = null
    private var writer: ObjectWriter? = null

    private val objectReader: ObjectReader?
        get() {
            if (reader == null) instantiateMapper()
            return reader
        }

    private val objectWriter: ObjectWriter?
        get() {
            if (writer == null) instantiateMapper()
            return writer
        }

    @Throws(IOException::class)
    fun fromJsonString(json: String): Any? {
        return objectReader!!.readValue<Any>(json)
    }

    @Throws(JsonProcessingException::class)
    fun toJsonString(obj: Array<DatabaseUser>): String {
        return objectWriter!!.writeValueAsString(obj)
    }

    private fun instantiateMapper() {
        val mapper = ObjectMapper()
        reader = mapper.reader(Array<DatabaseUser>::class.java)
        writer = mapper.writerFor(Array<DatabaseUser>::class.java)
    }
}